package com.weliyt.igdl.model;

import lombok.Data;

import java.util.Collections;
import java.util.Set;

@Data
public class SimpleResponse {

    private Set<String> imgUrls = Collections.emptySet();
    private Set<String> vidUrls = Collections.emptySet();

}
