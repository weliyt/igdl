package com.weliyt.igdl.service;

import com.weliyt.igdl.entity.AccessLogEntry;
import com.weliyt.igdl.repo.AccessLogRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

@Slf4j
@Service
public class LogService {

    private final boolean IS_DB_LOG_ENABLED;
    private final AccessLogRepo accessLogRepo;

    public LogService(@Value("${access-log-db.enabled:false}") final boolean dbLogEnabled,
                      final AccessLogRepo accessLogRepo) {
        this.IS_DB_LOG_ENABLED = dbLogEnabled;
        this.accessLogRepo = accessLogRepo;
    }

    @Async
    public void doLog(final AccessLogEntry entry) {
        log.info(entry.getSimplifiedString());
        if (IS_DB_LOG_ENABLED) {
            accessLogRepo.save(entry);
        }
    }

    @Async
    public void doLog(final String origin, final String requested) {
        var entry = new AccessLogEntry();
        entry.setDatetime(LocalDateTime.now(ZoneOffset.UTC));
        entry.setOrigin(origin);
        entry.setRequestedUrl(requested);
        doLog(entry);
    }

}
