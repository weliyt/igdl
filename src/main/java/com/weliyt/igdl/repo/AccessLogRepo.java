package com.weliyt.igdl.repo;

import com.weliyt.igdl.entity.AccessLogEntry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccessLogRepo extends JpaRepository<AccessLogEntry, Long> {

}
