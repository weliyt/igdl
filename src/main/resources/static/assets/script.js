const regexIgPid = /^(?:https?:\/\/)?(?:www\.)?instagram\.com\/p\/([^\?\/]+)/;

function doFetch() {
    let url = document.getElementById("txtUrl").value;
    if (url === "") return;

    let btn = document.getElementById("btnSubmit");
    btn.disabled = true;
    btn.textContent = "Working...";

    if (searchHistory(url)) {
        document.getElementById("holder").innerHTML = "";
        loadHistory();
        btn.disabled = false;
        btn.textContent = "Download";
    } else {
        $.ajax({
            url: "dofetch",
            method: "POST",
            data: url,
            contentType: "application/json",
            processData: false
        }).done(function (resp) {
            if (resp.imgUrls.length <= 0) return;

            let holder = document.getElementById("holder");
            let elems = createElements([{imgs: resp.imgUrls, vids: resp.vidUrls}]);
            elems.forEach(e => holder.prepend(e));

            mergeHistory(url, resp);

        }).always(function () {
            btn.disabled = false;
            btn.textContent = "Download";
        });
    }
}

function createElements(entries) {
    let ret = [];
    entries.forEach(e => {
        e.imgs.forEach(img => ret.push(createImageElement(img)));
        e.vids.forEach(vid => ret.push(createVideoElement(vid)));
    });
    return ret;
}

function createImageElement(url) {
    let a = document.createElement("a");
    let img = document.createElement("img");

    img.src = url;
    a.href = url;
    a.target = "_blank";

    a.appendChild(img);

    return a;
}

function createVideoElement(url) {
    let a = document.createElement("a");
    let vid = document.createElement("video");
    let src = document.createElement("source");

    a.href = url;
    a.target = "_blank";

    vid.autoplay = "autoplay";
    vid.controls = "controls";
    vid.loop = "loop";

    src.src = url;
    src.type = "video/mp4";

    vid.appendChild(src);
    a.appendChild(vid);

    return a;
}

function loadHistory() {
    let entries = localStorage.getItem("entries");
    if (entries) {
        entries = JSON.parse(entries);

        let elems = createElements(entries);

        let holder = document.getElementById("holder");
        elems.forEach(e => holder.append(e));
    }
    return false;
}

function searchHistory(igurl) {
    let pid = extractPid(igurl);

    let entries = localStorage.getItem("entries");
    if (entries) {
        entries = JSON.parse(entries);

        for (let i=0; i<entries.length; i++) {
            if (entries[i].pid === pid || entries[i].igurl === igurl) {
                entries.unshift(entries.splice(i, 1)[0]);
                localStorage.setItem("entries", JSON.stringify(entries));
                return true;
            }
        }
    }
    return false;
}

function mergeHistory(igurl, payload) {
    let resp = {
        igurl: igurl,
        pid: extractPid(igurl),
        imgs: payload.imgUrls,
        vids: payload.vidUrls
    };

    let entries = localStorage.getItem("entries");
    if (entries) {
        entries = JSON.parse(entries);
    } else {
        entries = [];
    }
    entries.unshift(resp);
    localStorage.setItem("entries", JSON.stringify(entries));
}

function clearHistory() {
    localStorage.removeItem("entries");
    document.getElementById("holder").innerHTML = "";
}

function groomHistory() {
    let entries = localStorage.getItem("entries");
    if (entries) {
        if (entries === "null") {
            localStorage.removeItem("entries");
            return;
        }

        entries = JSON.parse(entries);

        entries.forEach(entry => {
            if (!entry.pid) {
                entry.pid = extractPid(entry.igurl);
            }
        })
        localStorage.setItem("entries", JSON.stringify(entries));
    }
}

function extractPid(igurl) {
    let matched = igurl.match(regexIgPid);
    if (matched.length === 2) {
        return matched[1];
    } else {
        return null;
    }
}